import sys
import os
import glob
import csv
from collections import defaultdict, Counter, OrderedDict
#print (sys.version)


#Variables
numCertified = 0  #number of Certified applications

status_num = 0    #column number of 'CASE_STATUS' (application status)
socName_num = 0   #column number of 'SOC_NAME' (occupation name)
socCode_num = 0   #column number of 'SOC_CODE' (soc code - occupation code)
state_num = 0     #column number of 'WORKSITE_STATE' (state of work location)

socName_count = Counter()    #tracks the count of occupation names
socCode_count = Counter()    #tracks the count of soc codes
state_count = Counter()      #tracks the count of state of work location
codeMap = defaultdict(list)  #mapping of SOC codes to there occupation names


#read the input file and gets the counts of the states, occupation for all the certified applications
#Function Input: input_filepath- filepath and name of the input file to analyse
def readInputFile(input_filepath):
    print("Input File: ", input_filepath)
    f = open(input_filepath, 'rt') 
    try:
        reader = csv.reader(f, delimiter=';') #reads the csv file and breaks the string of each row at ;
        header = next(reader) #gets the column titles 
        #print("Length of Header: ", len(header))

        #gets the column numbers of the required fields (which are: status, soc code, soc name, state)
        for i in range(0, len(header)):
            if 'status' in header[i].lower():
                status_num = i
                print(header[i], status_num)
                continue
            if 'soc_code' in header[i].lower():
                socCode_num = i
                print (header[i], socCode_num)
                continue
            if 'soc_name' in header[i].lower():
                socName_num = i
                print(header[i], socName_num)
                continue
            if ('work' in header[i].lower()) and ('state' in header[i].lower()) and ('2' not in header[i].lower()):
                state_num = i
                print(header[i], state_num)
                continue

        #get the counts of the neccessary data
        for row in reader:
            if row[status_num].lower() == 'certified':
                global numCertified 
                numCertified += 1 #keeps track of the total number of certified applications
                state_count[row[state_num]] += 1 

                code = row[socCode_num]
                name = row[socName_num]
                socCode_count[code] += 1
                socName_count[name] += 1
        
                #mapping SOC code to the occupation name
                if code in codeMap.keys():
                    if name not in codeMap[code]:
                        codeMap[code].append(name)
                else:
                    codeMap[code].append(name)
    finally:
        f.close() #closed the input file


#Returns the top 10 states 
#Function Input: state_count- count of how many times a state was reported as the workplace location for certified apps
def analyseState(state_count):
    top10 = list() 
    order_state_count = OrderedDict(sorted(state_count.items(), key=lambda t: t[0])) #alphabetizes dict containing the state counts
    top_state = dict(Counter(order_state_count).most_common(10)) #gets the top 10 most common states

    #calculating the percentage of certified applications in a state compared to total certified applications
    for i in top_state.keys():
        percentage = round((top_state[i]/numCertified) * 100, 1) 
        top10.append((i, top_state[i], percentage)) 
    return (top10)


#returns the top 10 occupations
#Function Inputs: socCode_count- count of how many time a soc_code was reported for certified apps; codeMap- mapping of codes to occupation titles
def analyseOccupation(socCode_count, codeMap):
    top10 = list()
    socCode_name_count = Counter() 
    
    #using the codeMap and create duplicate map of socCode_count with map keys mapped occupation names 
    for i in socCode_count.keys():
        value = codeMap[i]
        socCode_name_count[value[0]] = socCode_count[i] 

    order_socCode_name_count = OrderedDict(sorted(socCode_name_count.items(), key=lambda t: t[0])) #alphabetizes the code counts based on occupation name
    top_socCode_name = dict(Counter(order_socCode_name_count).most_common(10)) #gets the top 10 common occupations

    #calculating the percentage of certified application of a occupation to total certified applications
    for i in top_socCode_name.keys():
        percentage = round((top_socCode_name[i]/numCertified)*100, 1) 
        top10.append((i, top_socCode_name[i], percentage))  
    return (top10)


#writing the results of the top 10 into the appropriate files 
#Function Inputs: occupationFileType- boolean, True means writing out occupation results and False means writing out state results; top10_list- results to write out to file
def writeToFile(occupationFileType, top10_list):
    if occupationFileType:
        outputFile = sys.argv[2] #occupation results file path and name
        headerLine = 'TOP_OCCUPATIONS;NUMBER_CERTIFIED_APPLICATIONS;PERCENTAGE' #header for occupation results file
    else:
        outputFile = sys.argv[3] #state results file path and name
        headerLine = 'TOP_STATES;NUMBER_CERTIFIED_APPLICATIONS;PERCENTAGE' #header for state results file
    
    output = open(outputFile, 'w') 
    output.write(headerLine+'\n') #writing the header to the outputFile
    for line in top10_list:
        output.write(line[0]+';'+str(line[1])+';'+str(line[2])+'%\n') #writing each tuple in top10_list as a line into outputFile
    output.close
    

##STEP 1: Getting the input file
input_filepath = sys.argv[1] #input file path is passed in as an argument on the command line         
input_filenames = glob.glob("%s*.csv" %(input_filepath))   # imagine you're one directory above test dir
input_filepath = input_filenames[0]
#input_filename = os.path.basename(input_filepath)  #gets the file name from path 

##STEP 2: Reading and getting the appropriate data from the input file
readInputFile(input_filepath)
print ("Number of Certified Apps: ", numCertified)
print ("Length of State, SOC Code, SOC Name: ", len(state_count), len(socCode_count), len(socName_count))
print ("Length of codeMap: ", len(codeMap))

#3STEP 3: Analyzing the soc-code data and writing out the top10 occupations reported from certified apps
top10Occup_list = analyseOccupation(socCode_count, codeMap)
writeToFile(True, top10Occup_list)

##STEP 4: Analyzing the state data and writing out the top10 states reported from certified apps
top10State_list = analyseState(state_count)
writeToFile(False, top10State_list)






