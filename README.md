# h1b_statistics

## Problem
Analyze immigration data on H1B(A-1B, H-1B1, E-3) visa application processing. The data source if from the US Department of Labor and its [Office of Foreign Labor Certification Performance Data](https://www.foreignlaborcert.doleta.gov/performancedata.cfm#dis). 
Need to analyze past years data to calculate two metrics: **Top 10 Occupations** and **Top 10 States** for **certified** visa applications. 

## Approach
### Programing Language: python (version 3)
* Libraries used: sys, os, glob, csv, collections
### Input Data: a  `.csv` file from the  `input` directory
* Data Format in file: semicolon separated (";") format 
**Note:** The input folder should only contain one .csv data file. If there are multiple data files, the program will only analyse one of the .csv data file. 
### Output Data: two  `.txt`  files saved to the  `output`  directory
* `top_10_occupations.txt`: Top 10 occupations for certified visa applications
* `top_10_states.txt`: Top 10 states for certified visa applications
### Step 1: Reading the Data
The  `input` directory is passed in through the  `run.sh` file. From the directory the program gets a  `.csv` data file. Then the header row is obtained to determine which columns contine the data needed to analyse. Then for all the certified applications, we keep the count of the occupations and worksite states reported in hashmaps. 
* For occupations: the data if from the SOC_CODE column. The SOC_CODE column is used instead of SOC_NAME to avoid having miss calculations due to spelling mistakes. However, a second map is created to know the occupation name associated with the soc_code and for this mapping we look at the SOC_NAME. 
* For states: the data is for the state the applicant will work in so I am looking at the WORKSITE_STATE or LCA_CASE_WORKLOC1_STATE. For some years a secondary worksite state column is provided which the program ignores. The program only analysis the the first worksite location. 
### Step 2: Analyzing Top 10 Occupations
From the soc_code counts from step1, the program gets the top 10 most reported soc_code. Using the mapping between soc_code and soc_name, the occupation name is obtained for the top 10 soc_codes. Then the percentage is calculated and these results are writen out to the output file named `top_10_occupations.txt`.
### Step 3: Analyzing Top 10 States
Similar to step2, the states counts from step1 are used to get the top 10 states reported. Then the percentage is calculated. Then these results are writed to the output file named `top_10_states.txt`.

## Run-Time & Space
### Run-Time: O(n)
Most of time is spent reading the data because the program has to read each line of data to find all the certified application and get the appropriate data to analyse. So this take O(n) time where n is the number rows of data. Through out the program there are no nexted for-loops and after reading the data from file, the program never itterates through the whole data set, therefore resulting in O(n) time complexity. 
### Space
In the program, there are a lot of hashtables (or dictionary in python) are used. The largest map is the *codeMap* which creates a mapping between the SOC_CODE and SOC_NAMES. This *codeMap* has a length of about 800. The bigest any of these maps can get is as big as the input dataset. 

## Run Instructions
The terminal command to run the program is given in the __`run.sh`__ file. So to run the program just have run this file, __`./run.sh`__.
In the terminal command the input data file path is given and the output file names with path are given in the order of occupation and then state result outputs. 
**Note:** There are print statements in the program file which are logged in `logs.log` file in `src` directory to help de-bug and maintain the program. 

## Assumptions
1. There are certain assumptions made regarding the possible columns names for the data needed to analyse the data. If these assumptions are not met, then the program will fail. In that case, the assumptions need to be modified by changing the first 4 **if** statements in the **readInputFile** function. 
2. SOC_CODE have a 1-to-1 mapping tot he occupation names. That is one code is associated with exactly one unique occupation name. 
3. Data regarding the SOC_CODE and SOC_NAME is accurate. There are no checks in the program to catch and errors in the data. 
4. The appropriate input data file will be given. If this file is not formated they way mentioned above, then the program will fail. 





